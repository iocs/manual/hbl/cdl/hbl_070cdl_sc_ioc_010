###################################### ICS HWI ###############################################
#############################  ICS Instrument Library     ####################################
##  PLC Sample Code in VersionDog: ICS_LIBRARY_MASTER_PLC                                   ## 
##  CCDB device types: ICS_EH_PID                                                           ##  
##  EPICS HMI (Block Icons/Faceplates)@ GitLab.                                             ##
##                                                                                          ##  
##               CMDS_EH - Analog Heater with Current/Voltage control                       ##
##                                  based on ICS_EH_PID                                     ##  
##                                                                                          ##  
############################         Version: 1.5            ################################
# Author:  Wojciech Binczyk
# Date:    05-09-2024
# Version: v1.0
##############################################################################################



############################
#  STATUS BLOCK
############################
define_status_block()

#Operation modes
add_digital("OpMode_Auto",                                                        PV_DESC="Operation Mode Auto",       PV_ONAM="True",                              PV_ZNAM="False")
add_digital("OpMode_Manual",                                                      PV_DESC="Operation Mode Manual",     PV_ONAM="True",                              PV_ZNAM="False")
add_digital("OpMode_Forced",                                                      PV_DESC="Operation Mode Forced",     PV_ONAM="True",                              PV_ZNAM="False")

#Heater states
add_analog("HeaterResistance",         "REAL",  PV_PREC="2",   ARCHIVE=True,      PV_DESC="Heater Resistance",                 PV_EGU="ohm")
add_analog("HeaterCurrent",            "REAL",  PV_PREC="2",   ARCHIVE=True,      PV_DESC="Heater Current AI",                 PV_EGU="A")
add_analog("HeaterVoltage",            "REAL",  PV_PREC="2",   ARCHIVE=True,      PV_DESC="Heater Voltage AI",                 PV_EGU="V")
add_analog("HeaterPower",              "REAL",  PV_PREC="2",   ARCHIVE=True,      PV_DESC="Heater Power",                      PV_EGU="W")
add_analog("HeaterSP",                 "REAL",  PV_PREC="2",   ARCHIVE=True,      PV_DESC="Heater Setpoint",                   PV_EGU="W")
add_analog("HeaterMV",                 "REAL",  PV_PREC="2",   ARCHIVE=True,      PV_DESC="Heater Control")
add_digital("HeatingON",                                                          PV_DESC="Heater operating",          PV_ONAM="Heating",                           PV_ZNAM="NotHeating")
add_analog("PSU_Current",              "REAL",  PV_PREC="2",   ARCHIVE=True,      PV_DESC="Heater Current AI",                 PV_EGU="A")
add_analog("PSU_Voltage",              "REAL",  PV_PREC="2",   ARCHIVE=True,      PV_DESC="Heater Voltage AI",                 PV_EGU="V")
add_analog("PSU_Power",                "REAL",  PV_PREC="2",   ARCHIVE=True,      PV_DESC="Heater Power",                      PV_EGU="W")
add_analog("PSU_SP",  	               "REAL",  PV_PREC="2",   ARCHIVE=True,      PV_DESC="Heater Setpoint",                   PV_EGU="W")

#Setpoint HMI Limit
add_analog("SPHiLim",                  "REAL" ,                                   PV_DESC="Maximum for SP Spinner on HMI")
add_analog("SPLoLim",                  "REAL" ,                                   PV_DESC="Minimum for SP Spinner on HMI")
add_analog("CMHiLim",                  "REAL" ,                                   PV_DESC="Maximum for Current Monitor")
add_analog("CMLoLim",                  "REAL" ,                                   PV_DESC="Minimum for Current Monitor")
add_analog("VMHiLim",                  "REAL" ,                                   PV_DESC="Maximum for Voltage Monitor")
add_analog("VMLoLim",                  "REAL" ,                                   PV_DESC="Minimum for Voltage Monitor")
add_analog("PHiLim",                   "REAL" ,                                   PV_DESC="Maximum for Power Monitor")
add_analog("PLoLim",                   "REAL" ,                                   PV_DESC="Minimum for Power Monitor")

#Interlock signals
add_digital("GroupInterlock",                                                     PV_DESC="Group Interlock",                 PV_ONAM="True",                              PV_ZNAM="False")
add_string("InterlockMsg", 39,        PV_NAME="InterlockMsg",                     PV_DESC="Interlock Message")
add_digital("HeatInterlock",                                    ARCHIVE=True,     PV_DESC="Heat Interlock",                  PV_ONAM="True",                              PV_ZNAM="False")

#for OPI visualization
add_digital("EnableAutoBtn",                                                      PV_DESC="Enable Auto Button",              PV_ONAM="True",                              PV_ZNAM="False")
add_digital("EnableManualBtn",                                                    PV_DESC="Enable Manual Button",            PV_ONAM="True",                              PV_ZNAM="False")
add_digital("EnableForcedBtn",                                                    PV_DESC="Enable Forced Button",            PV_ONAM="True",                              PV_ZNAM="False")

add_digital("LatchAlarm",                                                        PV_DESC="Latching of the alarms")
add_digital("GroupAlarm",                                                        PV_DESC="Group Alarm for OPI")

#Status for PSU Selection
add_digital("SingleLoad_RB",                                                     PV_DESC="Single Load mode Readback",        PV_ONAM="True",                              PV_ZNAM="False")
add_digital("DoubleLoad_RB",                                                     PV_DESC="Double Load mode Readback",        PV_ONAM="True",                              PV_ZNAM="False")
add_digital("Relay1_RB",                                                         PV_DESC="Heater 1 Relay Readback",          PV_ONAM="True",                              PV_ZNAM="False")
add_digital("Relay2_RB",                                                         PV_DESC="Heater 2 Relay Readback",          PV_ONAM="True",                              PV_ZNAM="False")


#iTest integration

add_digital("Ramping",                                          ARCHIVE=True,    PV_DESC="Ramping Indicator",                PV_ONAM="True",                              PV_ZNAM="False")
add_digital("RampSettingOK",                                                     PV_DESC="Ramping can be enabled",           PV_ONAM="True",                              PV_ZNAM="False")
add_digital("RampingUP",                                                         PV_DESC="Ramping UP",                       PV_ONAM="RampingUP",                         PV_ZNAM="NotMoving")
add_digital("RampingDOWN",                                                       PV_DESC="Ramping Down",                     PV_ONAM="RampingDOWN",                       PV_ZNAM="NotMoving")
add_digital("In_Scanning",                                      ARCHIVE=True,    PV_DESC="during operation",                 PV_ONAM="True",                      		  PV_ZNAM="False")
add_digital("Connected",                                        ARCHIVE=True,    PV_DESC="Connected",                        PV_ONAM="True",                              PV_ZNAM="False")

add_analog("PSU_Status", 		   "REAL",  PV_PREC="2", 		ARCHIVE=True,    PV_DESC="Heater operating" )
add_analog("PSU_MV",  	 		   "REAL",  PV_PREC="2", 		ARCHIVE=True,    PV_DESC="Heater Control AO",                 PV_EGU="W")

add_analog("MaxRampUPSpd",         "REAL",  PV_PREC="2",                         PV_DESC="Maximum Ramping UP Speed",          PV_EGU="W/s")
add_analog("MaxRampDNSpd",         "REAL",  PV_PREC="2",                         PV_DESC="Maximum Ramping DOWN Speed",        PV_EGU="W/s")
add_analog("ActRampSpeed",         "REAL",  PV_PREC="2",                         PV_DESC="Actual Ramping Speed",              PV_EGU="W/s")

add_analog("FB_RampUPTIME",        "INT",                                        PV_DESC="Ramping UP time",                   PV_EGU="sec")
add_analog("FB_RampUPRANGE",       "REAL",  PV_PREC="2",                         PV_DESC="Ramping UP range",                  PV_EGU="W")
add_analog("FB_RampDNTIME",        "INT",                                        PV_DESC="Ramping DOWN time",                 PV_EGU="sec")
add_analog("FB_RampDNRANGE",       "REAL",  PV_PREC="2",                         PV_DESC="Ramping DOWN range",                PV_EGU="W")
add_digital("FB_DoubleLoad",  	                                ARCHIVE=True,    PV_DESC="Double load configuration",         PV_ONAM="True",                            PV_ZNAM="False")
add_digital("FB_SingleLoad",  	                                ARCHIVE=True,    PV_DESC="Double load configuration",         PV_ONAM="True",                            PV_ZNAM="False")
add_digital("FB_Relay1",  	                                    ARCHIVE=True,    PV_DESC="relay 1 in use",                    PV_ONAM="True",                            PV_ZNAM="False")
add_digital("FB_Relay2",  	                                    ARCHIVE=True,    PV_DESC="relay 2 in use",                    PV_ONAM="True",                            PV_ZNAM="False")
add_digital("FB_DefaultMode",  	                                ARCHIVE=True,    PV_DESC="Default load mode",                 PV_ONAM="True",                            PV_ZNAM="False")

add_analog("EHResistanceScan1",    "REAL",  PV_PREC="2", 	    ARCHIVE=True,     PV_DESC="Heater resistance  1",             PV_EGU="Ohm")

add_analog("UnitID","INT",  	                                ARCHIVE=True,     PV_DESC="MB device Unit ID")
add_analog("PSU_MAX_SP",           "REAL",  PV_PREC="2", 	    ARCHIVE=True,     PV_DESC="Heater Voltage ",                  PV_EGU="V")
add_analog("PSU_MAX_CURR",  	   "REAL",  PV_PREC="2", 	    ARCHIVE=True,     PV_DESC="Heater Current ",                  PV_EGU="A")
add_analog("PSU_MAX_VOLT",  	   "REAL",  PV_PREC="2", 	    ARCHIVE=True,     PV_DESC="Heater Voltage ",                  PV_EGU="V")
add_analog("CMHiHiLim",            "REAL",                                        PV_DESC="Maximum Hardware for Current Monitor")
add_analog("CMLoLoLim",            "REAL",                                        PV_DESC="Minimum Hardware for Current Monitor")
add_analog("VMHiHiLim",            "REAL",                                        PV_DESC="Maximum Hardware for Voltage Monitor")
add_analog("VMLoLoLim",            "REAL",                                        PV_DESC="Minimum Hardware for Voltage Monitor")
add_digital("Remote",                                           ARCHIVE=True,     PV_DESC="check if in remote mode",          PV_ONAM="True",                            PV_ZNAM="Local")
add_digital("Threshold_ON",                                     ARCHIVE=True,     PV_DESC="Treshold_ON",                      PV_ONAM="ON",                            PV_ZNAM="OFF")

add_analog("ReadBack_Status",	   "WORD",  	                ARCHIVE=True,     PV_DESC="Status ReadBack")
add_analog("PowerCmd_Status",	   "WORD",  	                ARCHIVE=True,     PV_DESC="Status write cmd on")
add_analog("ParameterCmd_Status",  "WORD",  	                ARCHIVE=True,     PV_DESC="Status write cmd parameters")
add_analog("ReadBackErr_Status",   "WORD",  	                ARCHIVE=True,     PV_DESC="Error Status ReadBack")
add_analog("PowerCmdErr_Status",   "WORD",  	                ARCHIVE=True,     PV_DESC="Error Status write cmd on")
add_analog("ParamCmdErr_Status",   "WORD",  	                ARCHIVE=True,     PV_DESC="Error Status write cmd parameters")

add_analog("StatusRB_0",           "BYTE" ,                                       PV_DESC="PSU ReadBack Status byte 0")
add_analog("StatusRB_1",           "BYTE" ,                                       PV_DESC="PSU ReadBack Status byte 1")

add_string("ModelR", 39,PV_NAME="ModelR", PV_DESC="model ID")
add_string("SerialNr", 39,PV_NAME="SerialNr", PV_DESC="card serial number")

add_major_alarm("Resistance_Discrep",       "Heater Resistance Discrep",          PV_ZNAM="NominalState")
add_minor_alarm("SPLimitActive","SPLimitActive",                                  PV_ZNAM="NominalState")
add_minor_alarm("OpenLoop","Open loop alarm",                                     PV_ZNAM="NominalState")


#Alarm signals
add_major_alarm("Power_Discrep",       "Heater Power Discrepancy", ARCHIVE=True,         PV_DESC="Heater Power Discrepancy",            PV_ZNAM="NominalState")
add_major_alarm("IO_Error",            "HW IO Error",              ARCHIVE=True,         PV_DESC="HW IO Error",                         PV_ZNAM="NominalState")
add_major_alarm("Input_Module_Error",  "Input_Module_Error",                                                                            PV_ZNAM="NominalState")
add_major_alarm("Output_Module_Error", "Output_Module_Error",                                                                           PV_ZNAM="NominalState")
add_major_alarm("SSTriggered",         "SSTriggered",                                                                                   PV_ZNAM="NominalState")

#Discrepancy
add_analog("DiscrWatt",                "REAL" ,                             PV_DESC="Discrepancy In Watt",            PV_EGU="W")
add_time("DiscrTime",                                                       PV_DESC="Discrepancy Time Interval")

#Feedbacks
add_analog("FB_Setpoint",              "REAL" , ARCHIVE=True,               PV_DESC="FB Setpoint from HMI (SP)")
add_analog("FB_Manipulated",           "REAL" ,                             PV_DESC="FB Manipulated Value (AO)",             PV_EGU="W")
add_analog("FB_Step",                  "REAL" ,                             PV_DESC="FB Step Value for Heating/Cooling",     PV_EGU="W")
add_analog("FB_ForcePower",            "REAL" ,                             PV_DESC="FB Forced Power",                       PV_EGU="W")
add_analog("FB_ForceCurrent",          "REAL" ,                             PV_DESC="FB Forced Current",                     PV_EGU="A")
add_analog("FB_ForceVoltage",          "REAL" ,                             PV_DESC="FB Forced Voltage",                     PV_EGU="V")

#Standby Synchroniztaion
add_digital("FB_SS_State",                                                  PV_DESC="Standby Synchroniztaion State")
add_analog("FB_SS_Value",              "REAL" ,                             PV_DESC="Standby Synchroniztaion Value", PV_EGU="%")



############################
#  COMMAND BLOCK
############################
define_command_block()

#OPI buttons
add_digital("Cmd_Auto",                                                     PV_DESC="CMD: Auto Mode")
add_digital("Cmd_Manual",                                                   PV_DESC="CMD: Manual Mode")
add_digital("Cmd_Force",                                                    PV_DESC="CMD: Force Mode")
add_digital("Cmd_ForceValInp",                                              PV_DESC="CMD: Force Input Values")
add_digital("Cmd_ForceValOut",                                              PV_DESC="CMD: Force Output Values")
add_digital("Cmd_AckAlarm",                                                 PV_DESC="CMD: Acknowledge Alarm")
add_digital("Cmd_SingleLoad_ON",                                            PV_DESC="CMD: Single Load Mode Selection")
add_digital("Cmd_DoubleLoad_ON",                                            PV_DESC="CMD: Double Load Mode Selection")
add_digital("Cmd_Heater1Only_ON",                                           PV_DESC="CMD: Only Heater1 Selection")
add_digital("Cmd_Heater2Only_ON",                                           PV_DESC="CMD: Only Heater2 Selection")


#iTest integration 
add_digital("Cmd_Connected",          			            ARCHIVE=True,   PV_DESC="Cmd Force conncet")
add_digital("Cmd_Clear_faults",                             ARCHIVE=True,   PV_DESC="clearing MB client faults")
add_digital("Cmd_Diag",                                     ARCHIVE=True,   PV_DESC="get diagnostics button")
add_digital("Cmd_RampON",                                   ARCHIVE=True,   PV_DESC="Turn Ramping ON")
add_digital("Cmd_RampOFF",                                  ARCHIVE=True,   PV_DESC="Turn Ramping OFF")
add_digital("Cmd_ON",                                       ARCHIVE=True,   PV_DESC="CMD: ON Mode")
add_digital("Cmd_OFF",                                      ARCHIVE=True,   PV_DESC="CMD: OFF Mode")
add_digital("Cmd_DoubleLoad",                               ARCHIVE=True,   PV_DESC="Cmd Double load")
add_digital("Cmd_SingleLoad",                               ARCHIVE=True,   PV_DESC="Cmd single load")
add_digital("Cmd_Relay1",                                   ARCHIVE=True,   PV_DESC="relay 1 ON/OFF")
add_digital("Cmd_Relay2",                                   ARCHIVE=True,   PV_DESC="relay 2 ON/OFF")
add_digital("Cmd_Threshold_ON",                             ARCHIVE=True,   PV_DESC="command treshold ON")
add_digital("Cmd_Threshold_OFF",                            ARCHIVE=True,   PV_DESC="command treshold OFF")
add_digital("Cmd_Scan",                                     ARCHIVE=True,   PV_DESC="Scan loads")


############################
#  PARAMETER BLOCK
############################
define_parameter_block()

#PID

#Setpoint and Manipulated value from HMI
add_analog("P_Setpoint",               "REAL" ,                             PV_DESC="Setpoint from HMI (SP)")

#Step value when pressing Cmd_Open1Step or Cmd_Close1Step
add_analog("P_Step",                  "REAL" ,                             PV_DESC="Step value for SetPoint")
add_analog("P_ForcePower",            "REAL" ,                             PV_DESC="Forced Power",                       PV_EGU="W")
add_analog("P_ForceCurrent",          "REAL" ,                             PV_DESC="Forced Current",                     PV_EGU="A")
add_analog("P_ForceVoltage",          "REAL" ,                             PV_DESC="Forced Voltage",                     PV_EGU="V")

#iTest integration 

add_digital("P_Manual_Interlock",                             ARCHIVE=True,	PV_DESC="High inductance")
add_analog("P_RampUPTIME",				"INT",                ARCHIVE=True, PV_DESC="Ramping UP Time",                          PV_EGU="sec")
add_analog("P_RampUPRANGE",				"REAL",  PV_PREC="2", ARCHIVE=True,	PV_DESC="Ramping UP Range",                         PV_EGU="W")
add_analog("P_RampDNTIME",				"INT",                ARCHIVE=True, PV_DESC="Ramping DOWN Time",                        PV_EGU="sec")
add_analog("P_RampDNRANGE",				"REAL",  PV_PREC="2", ARCHIVE=True, PV_DESC="Ramping DOWN Range",                       PV_EGU="W")
add_analog("P_CMHiLim",                 "REAL",                             PV_DESC="Maximum for Current ")
add_analog("P_CMHiHiLim",               "REAL",                             PV_DESC="Maximum Hardware for Current ")
add_analog("P_CMLoLim",                 "REAL",                             PV_DESC="Minimum for Current ")
add_analog("P_CMLoLoLim",               "REAL",                             PV_DESC="Minimum Hardware for Current ")
add_analog("P_VMHiLim",                 "REAL",                             PV_DESC="Maximum for Voltage ")
add_analog("P_VMHiHiLim",               "REAL",                             PV_DESC="Maximum Hardware for Voltage ")
add_analog("P_VMLoLim",                 "REAL",                             PV_DESC="Maximum for Voltage ")
add_analog("P_VMLoLoLim",               "REAL",                             PV_DESC="Minimum Hardware for Voltage ")
